#!/bin/bash
logfile=/var/atlassian/application-data/jira/log/atlassian-jira.log

echo ""
echo "Waiting for JIRA to start..."
echo ""

if [ -f $logfile ]; then
    echo "JIRA has already been started before. No need to initialize."
    echo ""
else
    # Start JIRA
    su -c "/opt/atlassian/jira/bin/start-jira.sh" jira

    # Wait for the logfile to show up
    while [ ! -f /var/atlassian/application-data/jira/log/atlassian-jira.log ];
    do
        sleep 1
    done

    # Give a few seconds to finish startup
    sleep 7

    echo "JIRA has been initialized...shutting down"
    echo ""

    # Stop JIRA
    su -c "/opt/atlassian/jira/bin/stop-jira.sh" jira && \
    echo "JIRA has stopped"
    echo ""
fi
