################
# JIRA Image
################

# Set the base image
FROM centos:7.2.1511

# File Author / Maintainer
MAINTAINER orion-pax dkhaws77@gmail.com

# Build Args
ARG JIRA_VERSION
ENV JIRA_VERSION $JIRA_VERSION

## INFO
# app.jiraHome=/var/atlassian/application-data/jira
# sys.installationDir=/opt/atlassian/jira/atlassian-jira/WEB-INF
##

# Add the necessary files
COPY response.varfile /response.varfile
RUN chmod 644 /response.varfile

# Install Jira
RUN \
curl -L https://api.bitbucket.org/2.0/repositories/%7Bec20011b-2903-4c97-bfa3-5ad0ad278dc9%7D/%7Bfac6fa27-14cf-4da8-beeb-12f760f5916e%7D/downloads/atlassian-jira-software-${JIRA_VERSION}-x64.bin > /atlassian-jira-software-${JIRA_VERSION}-x64.bin && \
chmod 744 /atlassian-jira-software-${JIRA_VERSION}-x64.bin && \
/atlassian-jira-software-${JIRA_VERSION}-x64.bin -q -varfile /response.varfile && \
rm -rf /atlassian-jira-software-${JIRA_VERSION}-x64.bin

####
## Post installation tasks
####

# Add the logos directory as it doesn't exist after standard installation
RUN \
mkdir /var/atlassian/application-data/jira/logos && \
chown jira:jira /var/atlassian/application-data/jira/logos && \
chmod 755 /var/atlassian/application-data/jira/logos

# Put the MySQL driver in the JIRA installation
COPY mysql-connector-java-5.1.36-bin.jar /opt/atlassian/jira/atlassian-jira/WEB-INF/lib/mysql-connector-java-5.1.36-bin.jar
RUN \
chown root:root /opt/atlassian/jira/atlassian-jira/WEB-INF/lib/mysql-connector-java-5.1.36-bin.jar && \
chmod 644 /opt/atlassian/jira/atlassian-jira/WEB-INF/lib/mysql-connector-java-5.1.36-bin.jar

# Start and stop JIRA to ensure all structures are created
COPY jira-init.sh /jira-init.sh
RUN \
chmod 755 /jira-init.sh
RUN \
/jira-init.sh

# Add the config file; db conn, license, etc
COPY dbconfig.xml_v6 /dbconfig.xml_v6
COPY dbconfig.xml_v7 /dbconfig.xml_v7
RUN \
chown jira:jira /dbconfig.xml_v6 && \
chown jira:jira /dbconfig.xml_v7 && \
chmod 644 /dbconfig.xml_v6 && \
chmod 644 /dbconfig.xml_v7

# Add our custom setenv.sh script
COPY setenv.sh_v6 /setenv.sh_v6
COPY setenv.sh_v7 /setenv.sh_v7
RUN \
chown root:root /setenv.sh_v6 && \
chown root:root /setenv.sh_v7 && \
chmod 755 /setenv.sh_v6 && \
chmod 755 /setenv.sh_v7

# Add our custom version specific server.xml files
COPY server.xml_v6 /server.xml_v6
COPY server.xml_v7 /server.xml_v7
RUN \
chown root:root /server.xml_v6 && \
chown root:root /server.xml_v7 && \
chmod 644 /server.xml_v6 && \
chmod 644 /server.xml_v7

# Change owner and permissions to avoid severe error in startup.
RUN \
chown root:jira /opt/atlassian/jira/conf && \
chmod 775 /opt/atlassian/jira/conf

# Add necessary utilities
RUN yum install -y nfs-utils &&\
yum clean all

# Add the config and start scripts
COPY docker-cmd.sh /docker-cmd.sh
RUN chmod 744 /docker-cmd.sh

COPY jira-cfg.sh /jira-cfg.sh
RUN chmod 744 /jira-cfg.sh

# Expose the Jira port
EXPOSE 8080

# Run this on container startup
CMD ["./docker-cmd.sh"]
