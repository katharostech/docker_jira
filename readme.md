# Build the version specific image
Building this image requires that you pass a version of the JIRA binary to the build command.  This version must coincide directly with the build number that resides in the downloads section of BitBucket for this Git repo.
The same version of the JIRA binary used (7.2.1 as in the example below), should be used to tag the Docker image produced.  Additional tags may be added as needed after the initial image is built.

```bash
docker build --build-arg JIRA_VERSION=7.2.1 -t katharostech/jira:7.2.1 .
```

# Run the container
Running the container can be done as follows.  Take note that if any of the "db" environment variables are missing from the run command of this container, the container will assume that this is a new instance and it will treat it as such.  You will be presented with the default installation wizard when navigating to the site. If this site will sit behind a reverse proxy for SSL Termination, you will need to provide the domain in the "ssl_term_domain" environment variable. Simply omit this variable if it does not apply to your instance. This feature was added since SSL termination is not handled by JIRA out-of-the-box, and additional config must be addressed accordingly.

```bash
docker run -h jira \
--name jira \
-v ktech-prod-jira-plugins:/var/atlassian/application-data/jira/plugins:Z \
-v ktech-prod-jira-data:/var/atlassian/application-data/jira/data:Z \
-v ktech-prod-jira-caches:/var/atlassian/application-data/jira/caches:Z \
-v ktech-prod-jira-logos:/var/atlassian/application-data/jira/logos:Z \
-e dbhost="mydbhost" \
-e dbport="3306" \
-e dbname="mydb" \
-e dbuser="mydbuser" \
-e dbpass="mydbpass" \
-e ssl_term_domain="jira.katharostech.com" \
-p 8080:8080 \
-dt katharostech/jira:7.2.1
```
