# Set variables
DBCONFIGLOC=/var/atlassian/application-data/jira
DBCONFIGFILE=${DBCONFIGLOC}/dbconfig.xml
DBHOST=${dbhost}
DBPORT=${dbport}
DBNAME=${dbname}
DBUSER=${dbuser}
DBPASS=${dbpass}

SVRCONFIGLOC=/opt/atlassian/jira/conf/
SVRCONFIGFILE=${SVRCONFIGLOC}/server.xml
ENVCONFIGLOC=/opt/atlassian/jira/bin/
ENVCONFIGFILE=${ENVCONFIGLOC}/setenv.sh

VSCHEME="https"
VPROXYNAME=${ssl_term_domain}
VPROXYPORT="443"

# Setup version specific configuration files
if [[ ${JIRA_VERSION:0:1} == "6" ]] ; then 
    # Use version 6 config files
    JIRAVERSION="6"
    cp -f /dbconfig.xml_v6 ${DBCONFIGFILE}
    cp -f /setenv.sh_v6 ${ENVCONFIGFILE}
    cp -f /server.xml_v6 ${SVRCONFIGFILE}
else
    # Use version 7 config files
    JIRAVERSION="7"
    cp -f /dbconfig.xml_v7 ${DBCONFIGFILE}
    cp -f /setenv.sh_v7 ${ENVCONFIGFILE}
    cp -f /server.xml_v7 ${SVRCONFIGFILE}
fi

# Replace config parameters
if [[ -n "${DBHOST}" && -n "${DBPORT}" && -n "${DBNAME}" && -n "${DBUSER}" && -n "${DBPASS}" ]] ; then
    sed -i "/url/s/dbhost/${DBHOST}/g" ${DBCONFIGFILE}
    sed -i "/url/s/dbport/${DBPORT}/g" ${DBCONFIGFILE}
    sed -i "/url/s/dbname/${DBNAME}/g" ${DBCONFIGFILE}
    sed -i "/username/s/dbuser/${DBUSER}/g" ${DBCONFIGFILE}
    sed -i "/password/s/dbpass/${DBPASS}/g" ${DBCONFIGFILE}
else
    rm -f ${DBCONFIGFILE}
fi

# Only supplying logic for SSL Termination for JIRA 7 for now
if [[ ${JIRAVERSION} == "7" ]]
then
    # Insert https scheme for SSL termination or remove options for non-terminating domains
    if [[ "${VPROXYNAME}" ]] ; then
        sed -i "/^scheme/s/vscheme/${VSCHEME}/g" ${SVRCONFIGFILE}
        sed -i "/^proxyName/s/vproxyname/${VPROXYNAME}/g" ${SVRCONFIGFILE}
        sed -i "/^proxyPort/s/vproxyport/${VPROXYPORT}/g" ${SVRCONFIGFILE}
    else
        # delete the lines from the file
        sed -i "/^scheme/d" ${SVRCONFIGFILE}
        sed -i "/^proxyName/d" ${SVRCONFIGFILE}
        sed -i "/^proxyPort/d" ${SVRCONFIGFILE}
    fi
fi

# Mount the persisted directories
if [[ -n "${nfs_mount_data_cmd}" && -n "${nfs_mount_caches_cmd}" && -n "${nfs_mount_plugins_cmd}" && -n "${nfs_mount_logos_cmd}" ]] ; then
    eval ${nfs_mount_data_cmd}
    eval ${nfs_mount_caches_cmd}
    eval ${nfs_mount_plugins_cmd}
    eval ${nfs_mount_logos_cmd}
fi
